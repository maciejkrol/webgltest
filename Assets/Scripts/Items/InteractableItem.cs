using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Voxbox.Input;

namespace Voxbox.Items
{

    public class InteractableItem : MonoBehaviour
    {
        [SerializeField] private GameObject[] hiddenItem;
        
        [SerializeField] private Material[] materials;

        private MeshRenderer[] renderers;
        private List<Material> itemMaterials = new();

        private UserInput userInput;
        
        private void OnEnable()
        {
            userInput.Enable();     
        }

        private void OnDisable()
        {
            userInput.Disable();
        }

        private void Awake()
        {
            userInput = new UserInput();
        }

        private void Start()
        {
            //GetItemMaterials(); //currently NA
            renderers = this.gameObject.GetComponentsInChildren<MeshRenderer>();
            
            HideItems();
        }

        private void Update()
        {
            RaycastPosition();
        }

        #region MOUSE_POSITION

        private void RaycastPosition()
        {
            Vector2 mousePos  = userInput.Mouse.Position.ReadValue<Vector2>();

            if (mousePos.x is > 0 and < 960 && mousePos.y is > 0 and < 600)
            {
                StartCapture();
            }
            else
            {
                StopCapture();
            }
        }

        private void StopCapture()
        {
#if UNITY_WEBGL && !UNITY_EDITOR
        WebGLInput.captureAllKeyboardInput = false;
#endif
        }
            
        private void StartCapture()
        {
            
#if UNITY_WEBGL && !UNITY_EDITOR
        WebGLInput.captureAllKeyboardInput = true;
#endif
        }


        #endregion
        
        #region MATERIAL_&&_ITEM_SETTERS

        public void SetAllRed()
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material = materials[0];
            }
        }

        public void SetAllGreen()
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material = materials[1];
            }
        }


        public void ShowItems()
        {
            for (int i = 0; i < hiddenItem.Length; i++)
            {
                hiddenItem[i].gameObject.SetActive(true);
            }
        }
        
        public void HideItems()
        {
            for (int i = 0; i < hiddenItem.Length; i++)
            {
                hiddenItem[i].gameObject.SetActive(false);
            }
        }
        #endregion
        
    }
}