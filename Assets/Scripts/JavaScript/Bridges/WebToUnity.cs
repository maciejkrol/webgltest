﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Voxbox.UI
{

    public class WebToUnity : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI infoText;
        
        public void DisplayMessage(string text)
        {
            infoText.text = text;
        }
    }
}