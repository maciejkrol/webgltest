﻿using System.Runtime.InteropServices;

public static class WebGLPluginJS
{
    [DllImport("__Internal")]
    public static extern void SendMessageToPage(string text);
    
    [DllImport("__Internal")]
    public static extern string GetTextValue();
}
