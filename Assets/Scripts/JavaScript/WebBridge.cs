﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Voxbox.Items;
using Voxbox.UI;

namespace Voxbox.Javascript
{
    public class WebBridge : MonoBehaviour
    {
        [SerializeField] private string customTextToWebPage;
        [SerializeField] private InteractableItem item;
        [SerializeField] private WebToUnity toUnity;
        [SerializeField] private TextMeshProUGUI textToUnity;
        
        public void ReceiveMessageFromPage(string text)
        {
            textToUnity.text = text;
        }
        
        public void SendMessageToPage(string text)
        {
            if (customTextToWebPage == String.Empty)
            {
                WebGLPluginJS.SendMessageToPage(text);
            }
            else
            {
                WebGLPluginJS.SendMessageToPage(customTextToWebPage); 
            }
        }

        public void SetMat1()
        {
            item.SetAllRed();
        }
        
        public void SetMat2()
        {
            item.SetAllGreen();
        }

        public void ShowItem()
        {
            item.ShowItems();
        }

        public void HideItems()
        {
            item.HideItems();
        }
    }
}