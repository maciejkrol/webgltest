using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Voxbox.Utils
{
    public class ItemAutoRotate : MonoBehaviour
    {
        public bool canRotate = true;

        [SerializeField] private GameObject itemToRotate;
        
        [SerializeField] private float rotationSpeed;
        [SerializeField] private Vector3 rotateDirection;

        private void Start()
        {
            if (itemToRotate == null)
                itemToRotate = this.gameObject;
        }

        private void Update()
        {
            if (canRotate == true)
            {
                itemToRotate.transform.Rotate(rotateDirection * rotationSpeed);
            }
        }
    }
}