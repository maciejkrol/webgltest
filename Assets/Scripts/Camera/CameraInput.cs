using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Voxbox.Items;

namespace Voxbox.Input
{
    public class CameraInput : MonoBehaviour
    {
        private class CameraState
        {
            public float yaw;
            public float pitch;
            public float roll;
            public float x;
            public float y;
            public float z;

            public void Translate(Vector3 translation)
            {
                Vector3 rotatedTranslation = Quaternion.Euler(pitch, yaw, roll) * translation;

                x += rotatedTranslation.x;
                y += rotatedTranslation.y;
                z += rotatedTranslation.z;
            }

            public void LerpTowards(CameraState target, float positionLerpPct, float rotationLerpPct)
            {
                yaw = Mathf.Lerp(yaw, target.yaw, rotationLerpPct);
                pitch = Mathf.Lerp(pitch, target.pitch, rotationLerpPct);
                roll = Mathf.Lerp(roll, target.roll, rotationLerpPct);
                
                x = Mathf.Lerp(x, target.x, positionLerpPct);
                y = Mathf.Lerp(y, target.y, positionLerpPct);
                z = Mathf.Lerp(z, target.z, positionLerpPct);
            }

            public void UpdateTransform(Transform t)
            {
                t.eulerAngles = new Vector3(pitch, yaw, roll);
                t.position = new Vector3(x, y, z);
            }
        }
        
        
        CameraState m_TargetCameraState = new CameraState();
        CameraState m_InterpolatingCameraState = new CameraState();

        [Header("Movement Settings")]
        public float boost = 3.5f;
        
        [Tooltip("Time it takes to interpolate camera position 99% of the way to the target."), Range(0.001f, 1f)]
        public float positionLerpTime = 0.2f;
        
        [Tooltip("Time it takes to interpolate camera rotation 99% of the way to the target."), Range(0.001f, 1f)]
        public float rotationLerpTime = 0.01f;

        [SerializeField] private InteractableItem item;
        private UserInput userInput;
        
        private void OnEnable()
        {
            userInput.Enable();     
        }

        private void OnDisable()
        {
            userInput.Disable();
        }

        private void Awake()
        {
            userInput = new UserInput();
        }

        private void Update()
        {
            Vector3 translation = Vector3.zero;
            
            // Translation
            translation = GetInputTranslationDirection() * Time.deltaTime;

            // Speed up movement when shift key press
            if (userInput.Keyboard.Speed.IsPressed())
            {
                translation *= 10.0f;
            }
            
            m_TargetCameraState.Translate(translation);

            // Framerate-independent interpolation
            var positionLerpPct = 1f - Mathf.Exp((Mathf.Log(1f - 0.99f) / positionLerpTime) * Time.deltaTime);
            var rotationLerpPct = 1f - Mathf.Exp((Mathf.Log(1f - 0.99f) / rotationLerpTime) * Time.deltaTime);
            m_InterpolatingCameraState.LerpTowards(m_TargetCameraState, positionLerpPct, rotationLerpPct);

            m_InterpolatingCameraState.UpdateTransform(transform);
            
        }
        
        private Vector3 GetInputTranslationDirection()
        {
            float scrollInput = userInput.Mouse.Scroll.ReadValue<float>();
            Vector3 direction = new Vector3();
            
            if ( scrollInput > 0)
            {
                direction += Vector3.forward * boost;
            }
            
            if ( scrollInput < 0)
            {
                direction += Vector3.back * boost;
            }
            
            if (userInput.Keyboard.Left.IsPressed())
            {
                direction += Vector3.left;
            }
            
            if (userInput.Keyboard.Right.IsPressed())
            {
                direction += Vector3.right;
            }
            
            if (userInput.Keyboard.Up.IsPressed())
            {
                direction += Vector3.down;
            }
            
            if (userInput.Keyboard.Down.IsPressed())
            {
                direction += Vector3.up;
            }
            
            return direction;
        }
    }
}