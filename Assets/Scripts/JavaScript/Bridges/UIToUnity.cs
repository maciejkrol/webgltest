﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Voxbox.Core.UI
{

    public class UIToUnity : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI infoText;
        
        public void DisplayMessage(string text)
        {
            infoText.text = text;
        }
    }
}