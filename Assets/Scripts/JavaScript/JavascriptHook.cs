﻿using TMPro;
using UnityEngine;
using Voxbox.Items;

namespace Voxbox.Core.Javascript
{
    public class JavascriptHook : MonoBehaviour
    {
        [SerializeField] private InteractableItem interactableItem;
        
        [SerializeField] private TextMeshProUGUI testTextMessage;

        
        public void SetAllRed()
        {
            interactableItem.SetAllRed();
        }
        
        public void SetAllGreen()
        {
            interactableItem.SetAllGreen();
        }

        public void SetTextMessage(string text)
        {
            testTextMessage.text = text;
        }
        
        
    }
}