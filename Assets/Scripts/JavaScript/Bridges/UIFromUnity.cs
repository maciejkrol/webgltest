﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Voxbox.Javascript;

namespace Voxbox.UI
{

    public class UIFromUnity : MonoBehaviour
    {
        [SerializeField] private string message;
        [SerializeField] private TextMeshProUGUI textToPage;

        [SerializeField] private WebBridge bridge;

        public void SendMessageToWebPage()
        {
            //var message = textToPage.text;

            bridge.SendMessageToPage(message);
        }
    }
}