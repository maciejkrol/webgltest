
using System;
using Cinemachine;
using UnityEngine;
using Voxbox.Input;
using Voxbox.Items;

namespace Voxbox.Core
{
    public class DataStorage : MonoBehaviour
    {
        #region INPUT_REFERENCES

        
        [SerializeField] private MouseAndTouchRaycastedRotation raycastControls;
        public MouseAndTouchRaycastedRotation RaycastControls => raycastControls;

        
        [SerializeField] private MouseRotate mouseRotate;
        public MouseRotate MouseRotate => mouseRotate;

        
        #endregion
        
        [SerializeField] private CinemachineVirtualCamera sceneCamera;
        public CinemachineVirtualCamera SceneCamera => sceneCamera;

        [SerializeField] private InteractableItem item;
        public InteractableItem Item => item;
        
        [field: Header("Player Input Select"), Space(2)]
        [field: SerializeField] public bool UseRaycastControls { get; private set; }

        public void ResetCameraPositionAndFov()
        {
            item.transform.rotation = Quaternion.Euler(Vector3.zero);
            sceneCamera.m_Lens.FieldOfView = 36f;
        }

        private void Update()
        {
            if (UseRaycastControls == true)
            {
                raycastControls.UpdateRaycastedRotation();
            }
            else
            {
                mouseRotate.UpdateViewRotation();
            }
        }
    }
}